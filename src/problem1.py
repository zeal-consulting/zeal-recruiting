import unittest

def is_leap(year):
    leap = False
    return leap

class LeapYearTest(unittest.TestCase):
    def test_is_leap(self):
        self.assertEqual(is_leap(2010), False)

    def test_2019(self):
        self.assertEqual(is_leap(2019), False)

    def test_2020(self):
        self.assertEqual(is_leap(2020), True)

    def test_case_2100(self):
        self.assertEqual(is_leap(2100), False)
        
if __name__ == '__main__':
    unittest.main()