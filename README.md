
# Problem 1

## Leap Year Funtion
We add a Leap Day on February 29, almost every four years. The leap day is an extra, or intercalary day and we add it to the shortest
month of the year, February.

In the Gregorian calendar three criteria must be taken into account to identify leap years:

   * The year can be evenly divided by 4, is a leap year, unless:
     * The year can be evenly divided by 100, it is NOT a leap year, unless:
        * The year is also evenly divisible by 400. Then it is a leap year.

This means that in the Gregorian calendar, the years 2000 and 2400 are leap years, while 1800, 1900, 2100, 2200, 2300 and 2500 are
NOT leap years.
# Task

You are given the year, and you have to write a function to check if the year is leap or not.
Note that you have to complete the function and remaining code is given as template.

## Input Format

Read y, the year that needs to be checked.

## Constraints

1900 <= Y <= 10^5

## Output Format

Output is taken care of by the template. Your function must return a boolean value (True/False)

## Sample Input 0

1990

### Sample Output 0

False

# Problem 2

## Count the letter occurence

A newly opened multinational brand has decided to base their company logo on the three most common characters in the company name. They are now trying out various combinations of company names and logos based on this condition. Given a string , which is the company name in lowercase letters, your task is to find the top three most common characters in the string.

*Print the three most common characters along with their occurrence count.

*Sort in descending order of occurrence count.

*If the occurrence count is the same, sort the characters in alphabetical order.

For example, according to the conditions described above,
    GOOGLE would have it's logo with the letters G, O, E

## Input Format

A single line of input containing the string ## S

## Constraints

3 < len(S) <= 10^4

## Output Format

Print the three most common characters along with their occurrence count each on a separate line.
Sort output in descending order of occurrence count.
If the occurrence count is the same, sort the characters in alphabetical order.

## Sample Input 0

aabbbccde

### Sample Output 0

b 3
a 2
c 2

### Explanation

aabbbccde

Here, b occurs 3 times. It is printed first.
Both a and c occur 2 times. So, a is printed in the second line and c in the third line because a comes before c in the alphabet.

Note: The string S has at least 3 distinct characters.